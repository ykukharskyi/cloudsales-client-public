pipeline {
    agent any
    stages {
        stage('Install') {
            steps {
                sh "${WORKSPACE}/npmw install"
            }
        }
    }
}