const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ConfigWebpackPlugin = require('config-webpack');

const sourcePath = path.resolve(__dirname, 'src');
const targetPath = path.resolve(__dirname, 'static');
const resourcesPath = path.resolve(__dirname, 'resources');

module.exports = {
  entry: {
    app: `${sourcePath}/index.jsx`
  },
  output: {
    filename: 'bundle.[chunkhash].js',
    path: targetPath,
    publicPath: '/'
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: `${resourcesPath}/template.html`,
      filename: 'index.html'
    }),
    new ConfigWebpackPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.jsx$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.(jpe?g|png|ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
        use: 'base64-inline-loader?limit=1000&name=[name].[chunkhash].[ext]'
      }
    ]
  }
};
