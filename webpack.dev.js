const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'development',
  devServer: {
    inline: true,
    contentBase: './dist',
    port: 3001,
    historyApiFallback: true,
    proxy: {
      '/catalog': {
        target: 'http://localhost:8051',
        secure: false
      }
    }
  }
});
