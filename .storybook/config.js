import { configure, addDecorator } from '@storybook/react';
import { GlobalFontsDecorator } from '../src/components/layout/global/GlobalFonts';

const req = require.context('../src/components', true, /\.stories\.jsx$/);

function loadStories() {
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);

addDecorator(GlobalFontsDecorator);
