import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { UPDATE_PRODUCTS, UPDATE_TOP_CATEGORIES } from '../constants';

const products = (state = [], action) => (
  (action.type === UPDATE_PRODUCTS) ? action.payload : state
);

const topCategories = (state = [], action) => (
  (action.type === UPDATE_TOP_CATEGORIES) ? action.payload : state
);

export default applyMiddleware(thunk)(createStore)(
  combineReducers({
    products,
    topCategories
  })
);
