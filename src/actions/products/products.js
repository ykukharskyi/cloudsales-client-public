import fetch from 'isomorphic-fetch';
import { UPDATE_PRODUCTS } from '../../constants';

const FIRST_PAGE = 0;
const LANDING_QUANTITY = 8;

export const loadLandingProducts = () => (dispatch) => {
  fetch(`${CONFIG.endpoints.catalog.landingProducts}?page=${FIRST_PAGE}&size=${LANDING_QUANTITY}`)
    .then(response => response.json())
    .then((json) => {
      dispatch({
        type: UPDATE_PRODUCTS,
        payload: json
      });
    });
};

export const loadProductsByCategory = categoryCode => (dispatch) => {
  fetch(`${CONFIG.endpoints.catalog.products}?categoryCode=${categoryCode}&page=${FIRST_PAGE}&size=${LANDING_QUANTITY}`)
    .then(response => response.json())
    .then((json) => {
      dispatch({
        type: UPDATE_PRODUCTS,
        payload: json
      });
    });
};

export const loadSearchProducts = query => (dispatch) => {
  fetch(`${CONFIG.endpoints.catalog.searchProducts}?query=${query}&page=${FIRST_PAGE}&size=${LANDING_QUANTITY}`)
    .then(response => response.json())
    .then((json) => {
      dispatch({
        type: UPDATE_PRODUCTS,
        payload: json
      });
    });
};
