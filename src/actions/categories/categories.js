import fetch from 'isomorphic-fetch';
import { UPDATE_TOP_CATEGORIES } from '../../constants';

export const loadTopCategories = () => (dispatch) => {
  fetch(CONFIG.endpoints.catalog.topCategories)
    .then(response => response.json())
    .then((json) => {
      dispatch({
        type: UPDATE_TOP_CATEGORIES,
        payload: json
      });
    });
};
