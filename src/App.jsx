import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import DefaultLayout from './containers/DefaultLayoutContainer';
import HomePage from './containers/HomePageContainer';
import SearchResultPage from './containers/SearchResultPageContainer';
import CategoryPageContainer from './containers/CategoryPageContainer';
import store from './store';

export default () => (
  <Provider store={store}>
    <Router>
      <DefaultLayout>
        <Route exact path="/" component={HomePage} />
        <Route path="/search" component={SearchResultPage} />
        <Route path="/category/:categoryCode" component={CategoryPageContainer} />
      </DefaultLayout>
    </Router>
  </Provider>
);
