import { connect } from 'react-redux';
import HomePage from '../components/pages/HomePage';
import { loadLandingProducts } from '../actions';

const mapStateToProps = state => ({
  products: state.products
});

const mapDispatchToProps = dispatch => ({
  loadProducts: () => dispatch(loadLandingProducts())
});

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
