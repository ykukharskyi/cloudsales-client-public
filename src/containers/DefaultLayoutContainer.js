import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import DefaultLayout from '../components/layout/DefaultLayout';
import { loadTopCategories } from '../actions';

const mapStateToProps = state => ({
  topCategories: state.topCategories
});

const mapDispatchToProps = dispatch => ({
  loadTopCategories: () => dispatch(loadTopCategories())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DefaultLayout));
