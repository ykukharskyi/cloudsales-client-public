import { connect } from 'react-redux';
import SearchResultPage from '../components/pages/SearchResultPage';
import { loadSearchProducts } from '../actions';

const mapStateToProps = state => ({
  products: state.products
});

const mapDispatchToProps = dispatch => ({
  loadProducts: query => dispatch(loadSearchProducts(query))
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchResultPage);
