import { connect } from 'react-redux';
import CategoryPage from '../components/pages/CategoryPage';
import { loadProductsByCategory } from '../actions';

const mapStateToProps = state => ({
  products: state.products
});

const mapDispatchToProps = dispatch => ({
  loadProductsByCategory: categoryCode => dispatch(loadProductsByCategory(categoryCode))
});

export default connect(mapStateToProps, mapDispatchToProps)(CategoryPage);
