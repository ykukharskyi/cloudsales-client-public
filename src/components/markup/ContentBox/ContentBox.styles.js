import styled, { css } from 'styled-components';

export default styled.div`
  width: calc(100% - 40px);
  padding: 10px 20px;
  border-radius: 5px;
  border: 1px solid #e1e1e1;
  background-color: #fff;
  
  ${({ marginTop }) => marginTop && css`
    margin-top: 25px;
  `}
  
  ${({ marginBottom }) => marginBottom && css`
    margin-bottom: 25px;
  `}
`;
