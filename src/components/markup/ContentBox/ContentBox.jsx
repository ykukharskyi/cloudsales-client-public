import React from 'react';
import PropTypes from 'prop-types';
import InfoBoxWrapper from './ContentBox.styles';

const ContentBox = (props) => {
  const { children } = props;
  return <InfoBoxWrapper {...props}>{children}</InfoBoxWrapper>;
};

ContentBox.propTypes = {
  children: PropTypes.node.isRequired,
  marginTop: PropTypes.bool,
  marginBottom: PropTypes.bool
};

ContentBox.defaultProps = {
  marginTop: false,
  marginBottom: false
};

export default ContentBox;
