import React from 'react';
import { storiesOf } from '@storybook/react';
import ContentBox from './ContentBox';

storiesOf('Markup/ContentBox', module)
  .add('default render', () => <ContentBox>Sample Content</ContentBox>)
  .add('with margins', () => <ContentBox marginTop marginBottom>Sample Content</ContentBox>);
