import React from 'react';
import { storiesOf } from '@storybook/react';
import ProductsGrid from './ProductsGrid';
import ProductTail from '../../complex/ProductTail';

const product = {
  title: 'Pablo Coelho jacket',
  category: 'Women’s Suit Jacket',
  image: 'https://www.soulrevolver.com/sitecontent/images/vm_product/resized/caferacer_black_leather_jacket_front_343x.jpg',
  price: 125.5,
  currency: '€'
};

storiesOf('Markup/ProductsGrid', module)
  .add('one product', () => (
    <ProductsGrid>
      <ProductTail product={product} />
    </ProductsGrid>
  ))
  .add('two products', () => (
    <ProductsGrid>
      <ProductTail product={product} />
      <ProductTail product={product} />
    </ProductsGrid>
  ))
  .add('ten products', () => (
    <ProductsGrid>
      <ProductTail product={product} />
      <ProductTail product={product} />
      <ProductTail product={product} />
      <ProductTail product={product} />
      <ProductTail product={product} />
      <ProductTail product={product} />
      <ProductTail product={product} />
      <ProductTail product={product} />
      <ProductTail product={product} />
      <ProductTail product={product} />
    </ProductsGrid>
  ));
