import React from 'react';
import PropTypes from 'prop-types';
import { ProductsGridWrapper, ProductTailWrapper } from './ProductsGrid.styles';

const ProductsGrid = ({ children }) => (
  <ProductsGridWrapper>
    {React.Children.map(children, productTail => (
      <ProductTailWrapper>
        {productTail}
      </ProductTailWrapper>
    ))}
  </ProductsGridWrapper>
);

ProductsGrid.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element)
  ]).isRequired
};

export default ProductsGrid;
