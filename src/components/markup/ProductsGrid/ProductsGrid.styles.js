import styled from 'styled-components';

export const ProductTailWrapper = styled.div``;

export const ProductsGridWrapper = styled.div`
  width: 100%;
  
  display: flex;
  flex-wrap: wrap;
  
  @media (min-width: 940px) {
    ${ProductTailWrapper} {
      width: calc(25% - 7.5px);
      padding-bottom: 10px;
    }
    ${ProductTailWrapper}:not(:nth-child(4n)) {
      padding-right: 10px;
    }
  }
`;
