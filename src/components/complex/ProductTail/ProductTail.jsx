import React from 'react';
import PropTypes from 'prop-types';
import ImageFrame from '../../common/images/ImageFrame';
import Link from '../../common/buttons/Link';
import ProductPrice from '../../common/text/ProductPrice';
import {
  ProductTailWrapper,
  ProductImageWrapper,
  ProductInfoWrapper,
  ProductPriceWrapper,
  InlineTextWrapper
} from './ProductTail.styles';

const ProductTail = ({ product }) => {
  const {
    title,
    category,
    image,
    price,
    currency
  } = product;
  const { title: categoryTitle } = category;
  return (
    <ProductTailWrapper>
      <ProductImageWrapper>
        <ImageFrame src={image} height={170} width={150} fit />
      </ProductImageWrapper>
      <ProductInfoWrapper>
        <InlineTextWrapper>
          <Link size="large">{title}</Link>
        </InlineTextWrapper>
        <InlineTextWrapper>
          <Link size="small" colorTheme="secondary">{categoryTitle}</Link>
        </InlineTextWrapper>
        <ProductPriceWrapper>
          <ProductPrice currency={currency}>{price}</ProductPrice>
        </ProductPriceWrapper>
      </ProductInfoWrapper>
    </ProductTailWrapper>
  );
};

ProductTail.propTypes = {
  product: PropTypes.shape({
    title: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    currency: PropTypes.string.isRequired,
    category: PropTypes.shape({
      title: PropTypes.string.isRequired
    }).isRequired
  }).isRequired
};

export default ProductTail;
