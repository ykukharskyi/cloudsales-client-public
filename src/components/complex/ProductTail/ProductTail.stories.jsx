import React from 'react';
import { storiesOf } from '@storybook/react';
import ProductTail from './ProductTail';

const product = {
  title: 'Pablo Coelho jacket',
  category: 'Women’s Suit Jacket',
  image: 'https://www.soulrevolver.com/sitecontent/images/vm_product/resized/caferacer_black_leather_jacket_front_343x.jpg',
  price: 125.5,
  currency: '€'
};

storiesOf('Complex/ProductTail', module)
  .add('default render', () => <ProductTail product={product} />);
