import styled from 'styled-components';

export const ProductTailWrapper = styled.div`
  height: 300px;
  width: 100%;
  min-width: 215px;
  position: relative;
  border-right: 1px solid #d8d6d4;
  border-bottom: 1px solid #d8d6d4;
  border-radius: 5px;
  background-color: #fff;
`;

export const ProductImageWrapper = styled.div`
  display: flex;
  justify-content: center;
  height: 185px;
  padding: 10px 15px;
`;

export const ProductInfoWrapper = styled.div`
  border-top: 1px solid #d8d6d4;
  background-color: inherit;
  
  position: absolute;
  padding: 15px;
  bottom: 0;
  left: 0;
  
  width: calc(100% - 30px);
`;

export const ProductPriceWrapper = styled.div`
  position: relative;
  text-align: right;
  padding: 0 2px 5px 5px;
`;

export const InlineTextWrapper = styled.div`
  overflow: hidden;
  white-space: nowrap;
`;
