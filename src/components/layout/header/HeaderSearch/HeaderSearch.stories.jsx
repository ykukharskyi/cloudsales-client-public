import React from 'react';
import { storiesOf } from '@storybook/react';
import HeaderSearch from './HeaderSearch';

const backgroundColorDecoratorStyles = {
  padding: '10px',
  backgroundImage: '-webkit-linear-gradient(bottom, #f0eae3 0%, #fbf9f7 100%)'
};

const BackgroundColorDecorator = contentFn => (
  <div style={backgroundColorDecoratorStyles}>{contentFn()}</div>
);

storiesOf('Layout/Header/HeaderSearch', module)
  .addDecorator(BackgroundColorDecorator)
  .add('default render', () => <HeaderSearch />);
