import React, { useState } from 'react';
import { FaSearch } from 'react-icons/fa';
import Link from '../../../common/buttons/Link';
import { HeaderSearchWrapper, InputStyled } from './HeaderSearch.styles';

const EMPTY_QUERY = '';

const HeaderSearch = (props) => {
  const [query, setQuery] = useState(EMPTY_QUERY);
  const searchPath = `/search${query !== EMPTY_QUERY ? `?query=${query}` : EMPTY_QUERY}`;
  return (
    <HeaderSearchWrapper>
      <InputStyled onChange={event => setQuery(event.target.value)} type="text" placeholder="Search..." />
      <Link to={searchPath} colorTheme="secondary" {...props}>
        <FaSearch />
      </Link>
    </HeaderSearchWrapper>
  );
};

export default HeaderSearch;
