import styled from 'styled-components';

export const InputStyled = styled.input`
  height: 16px;
  width: calc(100% - 55px);
  font-size: 15px;
  background-color: transparent;
  border: none;
  margin-right: 10px;
  
  &:focus {
    outline: none;
  }
`;

export const HeaderSearchWrapper = styled.div`
  height: 43px;
  width: 100%;

  background-color: #fff;
  border-radius: 25px;
  border-bottom: 1px solid grey;
  border-right: 1px solid grey;
  
  display: flex;
  justify-content: center;
  align-items: center;
`;
