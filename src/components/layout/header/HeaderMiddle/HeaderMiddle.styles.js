import styled from 'styled-components';

export default styled.div`
  height: 107px;
  padding: 20px;
  width: calc(100% - 40px);
  box-shadow: 1px 2px 2px #666;
  background-image: -webkit-linear-gradient(bottom, #f0eae3 0%, #fbf9f7 100%);
  
  display: flex;
  justify-content: space-between;
  align-items: center;
  
  & > *:not(:first-child) {
    margin-left: 20px;
  }
  & > *:not(:last-child) {
    margin-right: 20px;
  }
`;
