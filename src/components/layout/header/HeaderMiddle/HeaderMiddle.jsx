import React from 'react';
import ImageFrame from '../../../common/images/ImageFrame';
import HeaderSearch from '../HeaderSearch';
import HeaderMiddleWrapper from './HeaderMiddle.styles';

const HeaderMiddle = props => (
  <HeaderMiddleWrapper>
    <ImageFrame src="https://adamantium.sk/shoppie/html/images/img-shoppie.png" height={64} width={135} />
    <HeaderSearch {...props} />
  </HeaderMiddleWrapper>
);

export default HeaderMiddle;
