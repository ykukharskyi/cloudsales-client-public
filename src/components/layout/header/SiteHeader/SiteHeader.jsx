import React from 'react';
import HeaderTopMenu from '../HeaderTopMenu';
import HeaderMiddle from '../HeaderMiddle';
import HeaderMainMenu from '../HeaderMainMenu';

const SiteHeader = props => (
  <>
    <HeaderTopMenu />
    <HeaderMiddle {...props} />
    <HeaderMainMenu {...props} />
  </>
);

export default SiteHeader;
