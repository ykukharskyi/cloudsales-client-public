import styled, { css } from 'styled-components';

const backgroundImage = 'data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABQAAD/7gAOQWRvYmUAZMAAAAAB/9sAhAACAgICAgICAgICAwICAgMEAwICAwQFBAQEBAQFBgUFBQUFBQYGBwcIBwcGCQkKCgkJDAwMDAwMDAwMDAwMDAwMAQMDAwUEBQkGBgkNCwkLDQ8ODg4ODw8MDAwMDA8PDAwMDAwMDwwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAAgAAYDAREAAhEBAxEB/8QAXgABAQEAAAAAAAAAAAAAAAAABQQJAQEBAAAAAAAAAAAAAAAAAAACAxAAAQIEBwAAAAAAAAAAAAAAAQAREgMTBCFBYUJSBRURAQEAAwAAAAAAAAAAAAAAAAARUQIS/9oADAMBAAIRAxEAPwDd+so1SB62qFVgyop04uHWWABHq3Bi3ESog3FpYGObgp8a5DrbD//Z';

const marginMenuStyle = css`
  & > * {
    margin-left: 15px;
    margin-right: 15px;
  }
`;

export const HeaderTopMenuWrapper = styled.div`
  display: flex;
  justify-content: space-between;

  background: #f2eae2 url(${backgroundImage}) repeat-x top left; 
  width: calc(100% - 20px);
  height: 26px;
  padding: 3px 10px;
  box-shadow: 1px 2px 2px #666666;
`;

export const HeaderTopMenuLeftWrapper = styled.div`
  ${marginMenuStyle}
`;

export const HeaderTopMenuRightWrapper = styled.div`
  ${marginMenuStyle}
`;
