import React from 'react';
import Link from '../../../common/buttons/Link';
import { HeaderTopMenuWrapper, HeaderTopMenuLeftWrapper, HeaderTopMenuRightWrapper } from './HeaderTopMenu.styles';

const HeaderTopMenu = () => (
  <HeaderTopMenuWrapper>
    <HeaderTopMenuLeftWrapper>
      <Link colorTheme="default-underline" uppercase>Blog</Link>
      <Link colorTheme="default-underline" uppercase>About Us</Link>
      <Link colorTheme="default-underline" uppercase>My Account</Link>
    </HeaderTopMenuLeftWrapper>
    <HeaderTopMenuRightWrapper>
      <Link colorTheme="default-underline" uppercase>Log In</Link>
      <Link colorTheme="default-underline" uppercase>Register</Link>
      <Link colorTheme="default-underline" uppercase>EN</Link>
    </HeaderTopMenuRightWrapper>
  </HeaderTopMenuWrapper>
);

export default HeaderTopMenu;
