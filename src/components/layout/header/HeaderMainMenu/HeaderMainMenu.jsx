import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { HeaderMainMenuWrapper } from './HeaderMainMenu.styles';
import MenuItem from './MenuItem';

class HeaderMainMenu extends PureComponent {
  componentDidMount () {
    const { loadTopCategories } = this.props;
    loadTopCategories();
  }

  render () {
    const { topCategories } = this.props;
    return (
      <HeaderMainMenuWrapper>
        {topCategories.map(category => (
          <MenuItem key={category.id} category={category} />
        ))}
      </HeaderMainMenuWrapper>
    );
  }
}

HeaderMainMenu.propTypes = {
  loadTopCategories: PropTypes.func.isRequired,
  topCategories: PropTypes.arrayOf(PropTypes.shape).isRequired
};

export default HeaderMainMenu;
