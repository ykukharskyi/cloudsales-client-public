import styled from 'styled-components';

export const MenuItemWrapper = styled.div`
  display: inline-block;
  height: 100%;
  width: fit-content;
  position: relative;
`;

export const MenuItemHover = styled.div`
  height: 75px;
  width: 100%;
  position: absolute;
  top: -5px;
  background-color: #fff;
  border-radius: 5px 5px 0 0;
  box-shadow: -5px 0 3px -5px #999, 5px 0 3px -5px #999, inset 1px 1px 1px #fff;
  z-index: 2;
  display: none;
  ${MenuItemWrapper}:hover & {
    display: block;
  }
`;

export const MenuItemTextWrapper = styled.div`
  height: 100%;
  padding-left: 23px;
  padding-right: 23px;
  width: calc(100% - 46px);
  position: inherit;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 3;
  &:hover {
    cursor: pointer;
  }
  &:hover > *, ${MenuItemWrapper}:hover & > * {
    color: #ee3124;
  }
`;

export const SubMenuWrapper = styled.div`
  min-width: 100%;
  box-sizing: border-box;
  position: absolute;
  top: 58px;
  background-color: #fff;
  box-shadow: 1px 1px 2px #666, inset 1px 1px 2px #fff;
  border-radius: 5px;
  padding: 25px;
  display: none;
  z-index: 1;
  ${MenuItemWrapper}:hover & {
    display: block;
  }
`;

export const SubMenuTitleWrapper = styled.div`
  margin-bottom: 15px;
`;

export const SubMenuItemWrapper = styled.div`
  white-space: nowrap;
  padding-bottom: 5px;
`;
