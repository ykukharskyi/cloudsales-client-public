import React from 'react';
import PropTypes from 'prop-types';
import Link from '../../../common/buttons/Link';
import Title from '../../../common/text/Title';
import {
  MenuItemWrapper,
  MenuItemHover,
  MenuItemTextWrapper,
  SubMenuWrapper,
  SubMenuItemWrapper,
  SubMenuTitleWrapper
} from './MenuItem.styles';

const MenuItem = ({ category }) => {
  const { title, subcategories } = category;
  return (
    <MenuItemWrapper>
      <MenuItemHover />
      <MenuItemTextWrapper>
        <Link to={`/category/${category.code}`} colorTheme="white-default" size="large" uppercase>{title}</Link>
      </MenuItemTextWrapper>
      <SubMenuWrapper>
        <SubMenuTitleWrapper>
          <Title level={3} colorTheme="primary" uppercase strong>Categories</Title>
        </SubMenuTitleWrapper>
        {subcategories.map(subcategory => (
          <SubMenuItemWrapper key={subcategory.id}>
            <Link to={`/category/${subcategory.code}`} size="medium">{subcategory.title}</Link>
          </SubMenuItemWrapper>
        ))}
      </SubMenuWrapper>
    </MenuItemWrapper>
  );
};

MenuItem.propTypes = {
  category: PropTypes.shape({
    title: PropTypes.string.isRequired,
    subcategories: PropTypes.arrayOf(PropTypes.shape).isRequired
  }).isRequired
};

export default MenuItem;
