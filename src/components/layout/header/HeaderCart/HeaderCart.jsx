import React from 'react';
import {
  HeaderCartWrapper,
  HeaderCartButton,
  HighlightedText,
  DefaultTextn
} from './HeaderCart.styles';

const HeaderCart = () => (
  <HeaderCartWrapper>
    <HeaderCartButton>
      <HighlightedText>
        {'39'}
      </HighlightedText>
      <DefaultTextn>
        {' items | '}
      </DefaultTextn>
      <HighlightedText>
        {'350.00 uah'}
      </HighlightedText>
    </HeaderCartButton>
  </HeaderCartWrapper>
);

export default HeaderCart;
