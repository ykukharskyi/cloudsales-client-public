import styled from 'styled-components';

export const HeaderCartWrapper = styled.div`
  height: 43px;
  width: 205px;

  display: flex;
  justify-content: center;
  align-items: center;

  background-color: #fff;
  border-radius: 25px;
  
  border-bottom: 1px solid grey;
  border-right: 1px solid grey;
`;

export const HeaderCartButton = styled.div`
`;

export const HighlightedText = styled.span`
  color: #ee3124;
  font-weight: bold;
`;

export const DefaultTextn = styled.span`
  text-transform: uppercase;
`;
