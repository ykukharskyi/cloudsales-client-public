import React from 'react';
import { storiesOf } from '@storybook/react';
import HeaderCart from './HeaderCart';

const backgroundColorDecoratorStyles = {
  padding: '10px',
  backgroundImage: '-webkit-linear-gradient(bottom, #f0eae3 0%, #fbf9f7 100%)'
};

const BackgroundColorDecorator = contentFn => (
  <div style={backgroundColorDecoratorStyles}>{contentFn()}</div>
);

storiesOf('Layout/Header/HeaderCart', module)
  .addDecorator(BackgroundColorDecorator)
  .add('default render', () => <HeaderCart />);
