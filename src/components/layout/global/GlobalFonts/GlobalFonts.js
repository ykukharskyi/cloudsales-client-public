import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  body, input, textarea, button {
    @import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro');
    font-family: 'Source Sans Pro', sans-serif;
  }
`;
