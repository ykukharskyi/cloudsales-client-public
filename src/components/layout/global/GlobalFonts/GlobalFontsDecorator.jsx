import React from 'react';
import GlobalFonts from './GlobalFonts';

export default contentFn => (
  <>
    <GlobalFonts />
    {contentFn()}
  </>
);
