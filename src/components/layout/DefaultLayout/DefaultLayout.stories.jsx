import React from 'react';
import { storiesOf } from '@storybook/react';
import ProductsGrid from '../../markup/ProductsGrid';
import ProductTail from '../../complex/ProductTail';
import PageContentSlot from '../content/PageContentSlot';
import DefaultLayout from './DefaultLayout';

const product = {
  title: 'Pablo Coelho jacket',
  category: 'Women’s Suit Jacket',
  image: 'https://www.soulrevolver.com/sitecontent/images/vm_product/resized/caferacer_black_leather_jacket_front_343x.jpg',
  price: 125.5,
  currency: '€'
};

storiesOf('Layout/DefaultLayout', module)
  .add('default render', () => (
    <DefaultLayout>
      <PageContentSlot>
        <ProductsGrid>
          <ProductTail product={product} />
          <ProductTail product={product} />
        </ProductsGrid>
      </PageContentSlot>
    </DefaultLayout>
  ));
