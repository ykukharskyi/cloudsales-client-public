import React from 'react';
import PropTypes from 'prop-types';
import GlobalFonts from '../global/GlobalFonts';
import PageContentSlot from '../content/PageContentSlot';
import SiteHeader from '../header/SiteHeader';
import SiteFooter from '../footer/SiteFooter';
import DefaultLayoutWrapper, { DefaultLayoutContent } from './DefaultLayout.styles';

const DefaultLayout = (props) => {
  const { children } = props;
  return (
    <DefaultLayoutWrapper>
      <GlobalFonts />
      <DefaultLayoutContent>
        <SiteHeader {...props} />
        {children}
        <SiteFooter />
      </DefaultLayoutContent>
    </DefaultLayoutWrapper>
  );
};

DefaultLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.instanceOf(PageContentSlot),
    PropTypes.arrayOf(PageContentSlot)
  ]).isRequired
};

export default DefaultLayout;
