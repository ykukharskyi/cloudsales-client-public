import styled from 'styled-components';

export default styled.div`
  margin-top: 20px;
  padding: 20px 20px;
  position: relative;
  background-color: #fbf9f7;
  border-radius: 5px;
  box-shadow: 1px 3px 2px rgba(80, 80, 80, 0.5);
`;
