import React from 'react';
import PropTypes from 'prop-types';
import PageContentSlotWrapper from './PageContentSlot.styles';

const PageContentSlot = ({ children }) => (
  <PageContentSlotWrapper>{children}</PageContentSlotWrapper>
);

PageContentSlot.propTypes = {
  children: PropTypes.node.isRequired
};

export default PageContentSlot;
