import styled from 'styled-components';

import backgroundImage from '../../../../../resources/images/bg/bg-footer.jpg';

export const FooterTopWrapper = styled.div`
  width: calc(100% - 40px);
  padding: 20px;
  border-radius: 5px 5px 0 0;
  
  display: flex;
  justify-content: space-between;
  
  background: transparent url(${backgroundImage}) repeat top left;
`;

export const TitleWrapper = styled.div`
  margin-bottom: 15px;
`;

export const MenuSectionWrapper = styled.div`
  width: 17%;
`;

export const InfoSectionWrapper = styled.div`
  flex-grow: 1;
`;
