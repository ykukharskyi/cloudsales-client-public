import React from 'react';
import Title from '../../../common/text/Title';
import Text from '../../../common/text/Text';
import List, { ListItem } from '../../../common/text/List';
import {
  FooterTopWrapper,
  TitleWrapper,
  MenuSectionWrapper,
  InfoSectionWrapper
} from './FooterTop.styles';

export default () => (
  <FooterTopWrapper>
    <MenuSectionWrapper>
      <TitleWrapper>
        <Title level={3} colorTheme="white" strong uppercase>Our Support</Title>
      </TitleWrapper>
      <List listStyleType="square" listStyle="space-between" colorTheme="secondary-white">
        <ListItem>Delivery Information</ListItem>
        <ListItem>Privacy policy</ListItem>
        <ListItem>Terms & Condition</ListItem>
        <ListItem>Contact us</ListItem>
      </List>
    </MenuSectionWrapper>
    <MenuSectionWrapper>
      <TitleWrapper>
        <Title level={3} colorTheme="white" strong uppercase>Our Service</Title>
      </TitleWrapper>
      <List listStyleType="square" listStyle="space-between" colorTheme="secondary-white">
        <ListItem>My Account</ListItem>
        <ListItem>Order History</ListItem>
        <ListItem>Returns</ListItem>
        <ListItem>Site map</ListItem>
      </List>
    </MenuSectionWrapper>
    <MenuSectionWrapper>
      <TitleWrapper>
        <Title level={3} colorTheme="white" strong uppercase>Information</Title>
      </TitleWrapper>
      <List listStyleType="square" listStyle="space-between" colorTheme="secondary-white">
        <ListItem>About us</ListItem>
        <ListItem>Delivery information</ListItem>
        <ListItem>Privacy policy</ListItem>
        <ListItem>Terms & Condition</ListItem>
      </List>
    </MenuSectionWrapper>
    <InfoSectionWrapper>
      <TitleWrapper>
        <Title level={3} colorTheme="white" strong uppercase>Payment</Title>
      </TitleWrapper>
      <Text colorTheme="secondary" sizeVariant="small" textAlign="justify">
        Lorem ipsum dolor sit amet, de consectetur adipiscing elit
      </Text>
    </InfoSectionWrapper>
    <InfoSectionWrapper>
      <TitleWrapper>
        <Title level={3} colorTheme="white" strong uppercase>Newsletter</Title>
      </TitleWrapper>
      <Text colorTheme="secondary" sizeVariant="small" textAlign="justify">
        Lorem ipsum dolor sit amet, de consectetur adipiscing elit
      </Text>
    </InfoSectionWrapper>
  </FooterTopWrapper>
);
