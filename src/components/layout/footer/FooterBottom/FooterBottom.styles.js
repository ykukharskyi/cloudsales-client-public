import styled from 'styled-components';

import backgroundImage from '../../../../../resources/images/bg/bg-bottom.jpg';

export const FooterBottomWrapper = styled.div`
  height: 50px;
  width: calc(100% - 40px);
  padding-left: 20px;
  padding-right: 20px;
  
  display: flex;
  align-items: center;
  
  background: transparent url(${backgroundImage}) repeat top left;
`;
