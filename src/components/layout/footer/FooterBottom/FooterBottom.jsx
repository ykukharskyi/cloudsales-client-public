import React from 'react';
import Text from '../../../common/text/Text';
import { FooterBottomWrapper } from './FooterBottom.styles';

export default () => (
  <FooterBottomWrapper>
    <Text sizeVariant="small" colorTheme="secondary">All Rights Reserved</Text>
  </FooterBottomWrapper>
);
