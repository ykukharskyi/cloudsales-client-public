import React from 'react';
import FooterTop from '../FooterTop';
import FooterBottom from '../FooterBottom';
import { SiteFooterWrapper } from './SiteFooter.styles';

export default () => (
  <SiteFooterWrapper>
    <FooterTop />
    <FooterBottom />
  </SiteFooterWrapper>
);
