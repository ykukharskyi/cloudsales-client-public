import React from 'react';
import PropTypes from 'prop-types';
import { FaAngleDown } from 'react-icons/fa';
import { DualButtonLeft, DualButtonRight, DualButtonWrapper } from './DualButton.styles';

const DualButton = ({ children, icon: Icon }) => (
  <DualButtonWrapper>
    <DualButtonLeft>
      {children}
      <Icon />
    </DualButtonLeft>
    <DualButtonRight>
      <FaAngleDown />
    </DualButtonRight>
  </DualButtonWrapper>
);

DualButton.propTypes = {
  children: PropTypes.string.isRequired,
  icon: PropTypes.func
};

DualButton.defaultProps = {
  icon: () => <></>
};

export default DualButton;
