import React from 'react';
import { storiesOf } from '@storybook/react';
import { FaStackOverflow } from 'react-icons/fa';
import DualButton from './DualButton';

storiesOf('Common/DualButton', module)
  .add('default render', () => <DualButton>My Dual Button</DualButton>)
  .add('with icon', () => <DualButton icon={FaStackOverflow}>My Dual Button</DualButton>);
