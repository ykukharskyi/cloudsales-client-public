import styled from 'styled-components';

const DualButtonCommon = styled.button`
  color: #ffffff;

  background-image: linear-gradient(bottom, #7b7770 0%, #999999 100%);
  background-image: -o-linear-gradient(bottom, #7b7770 0%, #999999 100%);
  background-image: -moz-linear-gradient(bottom, #7b7770 0%, #999999 100%);
  background-image: -webkit-linear-gradient(bottom, #7b7770 0%, #999999 100%);
  background-image: -ms-linear-gradient(bottom, #7b7770 0%, #999999 100%);
  background-color: #999999; 
  &:hover {
    background-image: linear-gradient(bottom, #0f0f0f 0%, #3e3e3e 100%);
    background-image: -o-linear-gradient(bottom, #0f0f0f 0%, #3e3e3e 100%);
    background-image: -moz-linear-gradient(bottom, #0f0f0f 0%, #3e3e3e 100%);
    background-image: -webkit-linear-gradient(bottom, #0f0f0f 0%, #3e3e3e 100%);
    background-image: -ms-linear-gradient(bottom, #0f0f0f 0%, #3e3e3e 100%);
    background-color: #3e3e3e;
  }
`;

export const DualButtonLeft = styled(DualButtonCommon)`
  width: calc(100% - 30px);
  
  font-size: 15px;
  font-weight: 700;
  border: none;
  cursor: pointer;
  
  position: relative;
  padding: 9px 45px 9px 15px;
  display: block;
  margin: 0 30px 0 0;
  z-index: 2;

  -moz-border-radius: 5px 0 0 5px;
  -webkit-border-radius: 5px 0 0 5px;
  border-radius: 5px 0 0 5px;
  
  & > svg {
    position: absolute;
    padding-top: 11px;
    height: 40%;
    width: 50px;
    top: 0;
    right: 0;
  }
`;

export const DualButtonRight = styled(DualButtonCommon)`
  border-left: 1px solid rgba(255, 255, 255, 0.2);
  border-right: none;
  border-top: none;
  border-bottom: none;
  position: absolute;
  top: 0;
  right: 0;
  width: 30px;
  height: 100%;

  -moz-border-radius: 0 5px 5px 0;
  -webkit-border-radius: 0 5px 5px 0;
  border-radius: 0 5px 5px 0;
  
  cursor: pointer;
  z-index: 2;
  
  & > svg {
    position: absolute;
    top: 0;
    left: 0;
    height: 45%;
    width: 45%;
    padding-top: 35%;
    padding-left: 25%;
  }
`;

export const DualButtonWrapper = styled.div`
  width: 100%;
  
  display: inline-block;
  position: relative;
  vertical-align: middle;
  
  box-sizing: border-box;
`;
