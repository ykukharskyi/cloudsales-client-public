import React from 'react';
import { storiesOf } from '@storybook/react';
import Link from './Link';

storiesOf('Common/Link', module)
  .add('default render', () => <Link>My Button</Link>)
  .add('large', () => <Link size="large" transparent>My Button</Link>)
  .add('small', () => <Link size="small" transparent>My Button</Link>)
  .add('small secondary', () => <Link size="small" colorTheme="secondary" transparent>My Button</Link>)
  .add('small secondary white', () => <Link size="small" colorTheme="secondary-white" transparent>My Button</Link>)
  .add('default underline', () => <Link colorTheme="default-underline" transparent>My Button</Link>)
  .add('default underline uppercase', () => <Link colorTheme="default-underline" uppercase transparent>My Button</Link>);
