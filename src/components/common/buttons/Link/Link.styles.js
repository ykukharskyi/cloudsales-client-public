import styled, { css } from 'styled-components';
import { Link } from 'react-router-dom';

export const LinkWrapper = styled(Link)`
  text-decoration: none;

  ${props => props.colorTheme === 'default' && css`
    color: #3e3e3e;
    &:hover {
      color: #ee3124;
    }
  `}

  ${props => props.colorTheme === 'secondary' && css`
    color: #999;
    &:hover {
      color: #3e3e3e;
    }
  `}

  ${props => props.colorTheme === 'secondary-white' && css`
    color: #999;
    &:hover {
      color: #fff;
    }
  `}

  ${props => props.colorTheme === 'default-underline' && css`
    color: #3e3e3e;
    &:hover {
      text-decoration: underline;
    }
  `}
  
  ${props => props.colorTheme === 'white-default' && css`
    color: #fff;
    &:hover {
      color: #ee3124;
    }
  `}

  ${props => props.size === 'medium' && css`
    font-size: 14px;
  `}

  ${props => props.size === 'large' && css`
    font-size: 18px;
    font-weight: bold;
  `}

  ${props => props.size === 'small' && css`
    font-size: 11px;
  `}

  ${props => props.uppercase && css`
    text-transform: uppercase;
  `}
`;
