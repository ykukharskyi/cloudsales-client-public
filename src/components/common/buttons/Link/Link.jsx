import React from 'react';
import PropTypes from 'prop-types';
import { LinkWrapper } from './Link.styles';

const Link = (props) => {
  const { children, to } = props;
  return (
    <LinkWrapper to={to} {...props}>{children}</LinkWrapper>
  );
};

Link.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object
  ]).isRequired,
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  colorTheme: PropTypes.oneOf([
    'default',
    'secondary',
    'secondary-white',
    'default-underline',
    'white-default'
  ]),
  uppercase: PropTypes.bool,
  to: PropTypes.string
};

Link.defaultProps = {
  size: 'medium',
  colorTheme: 'default',
  uppercase: false,
  to: undefined
};

export default Link;
