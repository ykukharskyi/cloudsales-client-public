import React from 'react';
import { FaShoppingCart } from 'react-icons/fa';
import DualButton from '../DualButton';

export default () => (
  <DualButton icon={FaShoppingCart}>Add to cart</DualButton>
);
