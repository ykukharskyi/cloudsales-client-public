import React from 'react';
import { storiesOf } from '@storybook/react';
import CartButton from './CartButton';

storiesOf('Common/CartButton', module)
  .add('default render', () => <CartButton>My Button</CartButton>);
