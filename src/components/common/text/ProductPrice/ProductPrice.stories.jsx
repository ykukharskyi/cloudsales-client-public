import React from 'react';
import { storiesOf } from '@storybook/react';
import ProductPrice from './ProductPrice';

storiesOf('Common/ProductPrice', module)
  .add('without currency', () => <ProductPrice>{12.50}</ProductPrice>)
  .add('with currency', () => <ProductPrice currency="грн.">{12.50}</ProductPrice>)
  .add('with currency first', () => <ProductPrice currency="$" currencyFirst>{12.50}</ProductPrice>);
