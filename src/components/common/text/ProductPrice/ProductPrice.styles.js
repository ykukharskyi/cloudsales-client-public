import styled from 'styled-components';

export default styled.div`
  font-size: 20px;
  font-weight: bold;
  color: #ee3124;
`;
