import React from 'react';
import PropTypes from 'prop-types';
import ProductPriceWrapper from './ProductPrice.styles';

const ProductPrice = ({ children, currency, currencyFirst }) => {
  const price = children / 100;
  let formattedPrice = parseFloat(price).toFixed(2);
  if (currency) {
    if (currencyFirst) {
      formattedPrice = `${currency}${formattedPrice}`;
    } else {
      formattedPrice = `${formattedPrice} ${currency}`;
    }
  }
  return <ProductPriceWrapper>{formattedPrice}</ProductPriceWrapper>;
};

ProductPrice.propTypes = {
  children: PropTypes.number.isRequired,
  currency: PropTypes.string,
  currencyFirst: PropTypes.bool
};

ProductPrice.defaultProps = {
  currency: undefined,
  currencyFirst: false
};

export default ProductPrice;
