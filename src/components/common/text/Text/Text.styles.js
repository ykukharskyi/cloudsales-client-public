import styled, { css } from 'styled-components';

export const TextWrapper = styled.span`
  ${({ colorTheme }) => colorTheme === 'secondary' && css`
    color: #999;
  `}
  
  ${({ colorTheme }) => colorTheme === 'white' && css`
    color: #fff;
  `}
  
  ${({ sizeVariant }) => sizeVariant === 'small' && css`
    font-size: 12px;
  `}
  
  ${({ textAlign }) => textAlign === 'justify' && css`
    text-align: justify;
  `}
`;
