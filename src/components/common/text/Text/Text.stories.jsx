import React from 'react';
import { storiesOf } from '@storybook/react';
import Text from './Text';

const sampleText = 'The quick brown fox jumps over the lazy dog.';

storiesOf('Common/Text', module)
  .add('default', () => <Text>{sampleText}</Text>)
  .add('secondary', () => <Text colorTheme="secondary">{sampleText}</Text>)
  .add('white', () => <Text colorTheme="white">{sampleText}</Text>)
  .add('small', () => <Text sizeVariant="small">{sampleText}</Text>)
  .add('small', () => <Text sizeVariant="small">{sampleText}</Text>)
  .add('justify', () => <Text textAlign="justify">{sampleText}</Text>);
