import React from 'react';
import PropTypes from 'prop-types';
import { TextWrapper } from './Text.styles';

const Text = (props) => {
  const { children } = props;
  return (
    <TextWrapper {...props}>{children}</TextWrapper>
  );
};

Text.propTypes = {
  children: PropTypes.string.isRequired,
  colorTheme: PropTypes.oneOf(['default', 'secondary', 'white']),
  sizeVariant: PropTypes.oneOf(['default', 'small']),
  textAlign: PropTypes.oneOf(['left', 'justify'])
};

Text.defaultProps = {
  colorTheme: 'default',
  sizeVariant: 'default',
  textAlign: 'left'
};

export default Text;
