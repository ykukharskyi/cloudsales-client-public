import React from 'react';
import { storiesOf } from '@storybook/react';
import List from './List';
import ListItem from './ListItem';

storiesOf('Common/List', module)
  .add('default', () => (
    <List>
      <ListItem>Item 1</ListItem>
      <ListItem>Item 2</ListItem>
      <ListItem>Item 3</ListItem>
    </List>
  ))
  .add('square', () => (
    <List listStyleType="square">
      <ListItem>Item 1</ListItem>
      <ListItem>Item 2</ListItem>
      <ListItem>Item 3</ListItem>
    </List>
  ))
  .add('secondary', () => (
    <List colorTheme="secondary">
      <ListItem>Item 1</ListItem>
      <ListItem>Item 2</ListItem>
      <ListItem>Item 3</ListItem>
    </List>
  ))
  .add('secondary-white', () => (
    <List colorTheme="secondary-white">
      <ListItem>Item 1</ListItem>
      <ListItem>Item 2</ListItem>
      <ListItem>Item 3</ListItem>
    </List>
  ))
  .add('space-between', () => (
    <List listStyle="space-between">
      <ListItem>Item 1</ListItem>
      <ListItem>Item 2</ListItem>
      <ListItem>Item 3</ListItem>
    </List>
  ));
