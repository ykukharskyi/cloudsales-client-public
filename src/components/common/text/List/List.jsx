import React from 'react';
import PropTypes from 'prop-types';
import ListItem from './ListItem';
import { ListWrapper } from './List.styles';

const List = (props) => {
  const { children } = props;
  return (
    <ListWrapper {...props}>
      {children}
    </ListWrapper>
  );
};

List.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.instanceOf(ListItem),
    PropTypes.arrayOf(ListItem)
  ]).isRequired,
  listStyleType: PropTypes.oneOf(['circle', 'square']),
  listStyle: PropTypes.oneOf(['default', 'space-between']),
  colorTheme: PropTypes.oneOf(['default', 'secondary', 'secondary-white'])
};

List.defaultProps = {
  listStyleType: 'circle',
  listStyle: 'default',
  colorTheme: 'default'
};

export default List;
