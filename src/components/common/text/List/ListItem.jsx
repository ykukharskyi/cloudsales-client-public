import React from 'react';
import PropTypes from 'prop-types';
import { ListItemWrapper } from './ListItem.styles';

const ListItem = ({ children }) => (
  <ListItemWrapper>
    {children}
  </ListItemWrapper>
);

ListItem.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element)
  ]).isRequired
};

export default ListItem;
