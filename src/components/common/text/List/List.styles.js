import styled, { css } from 'styled-components';

const secondaryStyles = css`
  font-size: 12px;
  padding-left: 15px;
  & > li {
    color: #999
  }
`;

export const ListWrapper = styled.ul`
  margin: 0;

  list-style-type: ${({ listStyleType }) => listStyleType}
  
  ${({ colorTheme }) => colorTheme === 'secondary' && secondaryStyles}
  
  ${({ colorTheme }) => colorTheme === 'secondary-white' && css`
    ${secondaryStyles}
    & > li:hover {
      color: #fff;
    }
  `}
  
  ${({ listStyle }) => listStyle === 'space-between' && css`
    & > li {
      margin-bottom: 10px;
    }
  `}
  
`;
