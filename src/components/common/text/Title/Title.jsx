import React from 'react';
import PropTypes from 'prop-types';
import { TitleWrapper } from './Title.styles';

const Title = (props) => {
  const { children } = props;
  return (
    <TitleWrapper {...props}>{children}</TitleWrapper>
  );
};

Title.propTypes = {
  children: PropTypes.string.isRequired,
  level: PropTypes.number,
  strong: PropTypes.bool,
  uppercase: PropTypes.bool,
  colorTheme: PropTypes.oneOf(['default', 'primary', 'white'])
};

Title.defaultProps = {
  level: 1,
  strong: false,
  uppercase: false,
  colorTheme: 'default'
};

export default Title;
