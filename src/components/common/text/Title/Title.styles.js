import styled, { css } from 'styled-components';

export const TitleWrapper = styled.span`
  ${({ level }) => level === 3 && css`
    font-size: 15px;
  `}
  
  ${({ strong }) => strong && css`
    font-weight: bold;
  `}
  
  ${({ uppercase }) => uppercase && css`
    text-transform: uppercase;
  `}
  
  ${({ colorTheme }) => colorTheme === 'white' && css`
    color: #fff;
  `}
  
  ${({ colorTheme }) => colorTheme === 'primary' && css`
    color: #ee3124;
  `}
`;
