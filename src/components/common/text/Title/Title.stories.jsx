import React from 'react';
import { storiesOf } from '@storybook/react';
import Title from './Title';

storiesOf('Common/Title', module)
  .add('default', () => <Title>My Title</Title>)
  .add('level 3', () => <Title level={3}>My Title</Title>)
  .add('strong', () => <Title strong>My Title</Title>)
  .add('uppercase', () => <Title uppercase>My Title</Title>);
