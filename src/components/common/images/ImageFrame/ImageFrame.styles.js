import styled from 'styled-components';

export default styled.div`
  height: ${props => props.height}px;
  width: ${props => props.width}px;
  background-image: url(${props => props.src});
  background-position: center;
  background-repeat: no-repeat;
  background-size: ${props => (props.fit ? 'contain' : 'cover')};
`;
