import React from 'react';
import { storiesOf } from '@storybook/react';
import ImageFrame from './ImageFrame';

storiesOf('Common/ImageFrame', module)
  .add('default render', () => (
    <ImageFrame
      height={200}
      width={200}
      src="http://www.ruspeach.com/upload/iblock/a4f/a4f2ed0a26279e788f4a2c05e2a78616.jpg"
    />
  ))
  .add('fit image', () => (
    <ImageFrame
      height={200}
      width={200}
      src="http://www.hohmodrom.ru/upload/53140/projimg/114430/hohmodrom_95_orig1_noviyy_razmer.jpg"
      fit
    />
  ));
