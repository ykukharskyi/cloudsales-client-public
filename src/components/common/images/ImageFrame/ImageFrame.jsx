import React from 'react';
import PropTypes from 'prop-types';
import ImageFrameContainer from './ImageFrame.styles';

const ImageFrame = props => (
  <ImageFrameContainer {...props} />
);

ImageFrame.propTypes = {
  src: PropTypes.string.isRequired,
  height: PropTypes.number.isRequired,
  width: PropTypes.number.isRequired,
  fit: PropTypes.bool
};

ImageFrame.defaultProps = {
  fit: false
};

export default ImageFrame;
