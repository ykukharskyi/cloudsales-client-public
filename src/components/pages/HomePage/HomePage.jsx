import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import PageContentSlot from '../../layout/content/PageContentSlot';
import ProductsGrid from '../../markup/ProductsGrid';
import ProductTail from '../../complex/ProductTail';

const HomePage = ({ products, loadProducts }) => {
  useEffect(() => {
    loadProducts();
  }, []);

  return (
    <PageContentSlot>
      <ProductsGrid>
        {products.map(product => (
          <ProductTail key={product.id} product={product} />
        ))}
      </ProductsGrid>
    </PageContentSlot>
  );
};

HomePage.propTypes = {
  loadProducts: PropTypes.func.isRequired,
  products: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default HomePage;
