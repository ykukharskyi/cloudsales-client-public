import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { parseUrl } from 'query-string';
import PageContentSlot from '../../layout/content/PageContentSlot';
import ProductsGrid from '../../markup/ProductsGrid';
import ProductTail from '../../complex/ProductTail';
import ContentBox from '../../markup/ContentBox';

class SearchResultPage extends PureComponent {
  componentDidMount () {
    const { location, history } = this.props;
    this.updateSearch(location);
    this.unlistenHistory = history.listen((newLocation) => {
      this.updateSearch(newLocation);
    });
  }

  componentWillUnmount () {
    this.unlistenHistory();
  }

  updateSearch (location) {
    const { loadProducts } = this.props;
    const { query } = parseUrl(location.search);
    const { query: queryString } = query;
    this.query = queryString;
    loadProducts(queryString);
  }

  render () {
    const { query } = this;
    const { products } = this.props;
    const productsQuantity = products.length;
    const showProducts = productsQuantity > 0;
    return (
      <PageContentSlot>
        <ContentBox marginBottom={showProducts}>
          {showProducts
            ? `Found ${productsQuantity} results for "${query}"`
            : `No results found for "${query}"`
          }
        </ContentBox>
        {showProducts && (
          <ProductsGrid>
            {products.map(product => (
              <ProductTail key={product.id} product={product} />
            ))}
          </ProductsGrid>
        )}
      </PageContentSlot>
    );
  }
}

SearchResultPage.propTypes = {
  products: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number
    })
  ).isRequired,
  location: PropTypes.shape({
    search: PropTypes.string
  }).isRequired,
  history: PropTypes.shape({
    listen: PropTypes.func
  }).isRequired,
  loadProducts: PropTypes.func.isRequired
};

export default SearchResultPage;
