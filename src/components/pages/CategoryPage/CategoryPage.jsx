import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import PageContentSlot from '../../layout/content/PageContentSlot';
import ProductsGrid from '../../markup/ProductsGrid';
import ProductTail from '../../complex/ProductTail';

const CategoryPage = ({ products, loadProductsByCategory, match }) => {
  const { params } = match;
  useEffect(() => {
    loadProductsByCategory(params.categoryCode);
  }, [params]);

  return (
    <PageContentSlot>
      <ProductsGrid>
        {products.map(product => (
          <ProductTail key={product.id} product={product} />
        ))}
      </ProductsGrid>
    </PageContentSlot>
  );
};

CategoryPage.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      categoryCode: PropTypes.string.isRequired
    }).isRequired
  }).isRequired,
  products: PropTypes.arrayOf(PropTypes.object).isRequired,
  loadProductsByCategory: PropTypes.func.isRequired
};

export default CategoryPage;
